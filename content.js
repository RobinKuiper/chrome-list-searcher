chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  var foundStrings = [];
  request.strings.forEach(function (str) {
    var elements = document.querySelectorAll(':contains(' + str + ')');
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].textContent.indexOf(str) >= 0) {
        foundStrings.push(str);
        break;
      }
    }
  });
  sendResponse({ foundStrings: foundStrings });
});

console.log("yes")