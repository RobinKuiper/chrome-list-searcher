document.addEventListener("DOMContentLoaded", function () {
  console.log("Loaded")
  var searchBtn = document.getElementById("submit");
  searchBtn.addEventListener('click', function () {
    alert("click")
    var strings = document.getElementById('strings').value.split('\n').map(function (str) {
      return str.trim();
    }).filter(function (str) {
      return str.length > 0;
    });
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { strings: strings }, function (response) {
        var resultsDiv = document.getElementById('results');
        resultsDiv.innerHTML = '';
        if (response.foundStrings.length === 0) {
          resultsDiv.textContent = 'No strings found';
        } else {
          var ul = document.createElement('ul');
          response.foundStrings.forEach(function (str) {
            var li = document.createElement('li');
            li.textContent = str;
            ul.appendChild(li);
          });
          resultsDiv.appendChild(ul);
        }
      });
    });
  });
});